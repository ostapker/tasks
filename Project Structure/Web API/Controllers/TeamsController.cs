﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Interfaces;
using Common.DTOs.Team;
using Microsoft.AspNetCore.Mvc;

namespace Web_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<TeamDTO>>> Get()
        {
            return Ok(await _teamService.GetAllAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> GetById(int id)
        {

            return Ok(await _teamService.GetByIdAsync(id));

        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> Post([FromBody] TeamCreateDTO teamDTO)
        {

            var team = await _teamService.CreateAsync(teamDTO);
            return Created(new Uri($"api/Teams/{team.Id}"), team);

        }

        [HttpPut]
        public async Task<ActionResult<TeamDTO>> Put([FromBody] TeamUpdateDTO teamDTO)
        {

            return Ok(await _teamService.UpdateAsync(teamDTO));

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id)
        {

            await _teamService.DeleteByIdAsync(id);
            return NoContent();

        }

        [HttpGet("TeamsWithUsers")]
        public async Task<ActionResult<IEnumerable<TeamWithUsersDTO>>> GetTeamsWithUsers()
        {
            return Ok(await _teamService.GetTeamsWithUsers());
        }
    }
}