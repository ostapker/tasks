﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Interfaces;
using Common.ContractResolvers;
using Common.DTOs.Project;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Web_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<ProjectDTO>>> Get()
        {
            return Ok(await _projectService.GetAllAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> GetById(int id)
        {

            return Ok(await _projectService.GetByIdAsync(id));

        }

        [HttpPost]
        public async Task<ActionResult<ProjectDTO>> Post([FromBody] ProjectCreateDTO projectDTO)
        {

            var project = await _projectService.CreateAsync(projectDTO);
            return Created(new Uri($"api/Projects/{project.Id}"), project);

        }

        [HttpPut]
        public async Task<ActionResult<ProjectDTO>> Put([FromBody] ProjectUpdateDTO projectDTO)
        {

            return Ok(await _projectService.UpdateAsync(projectDTO));

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id)
        {

            await _projectService.DeleteByIdAsync(id);
            return NoContent();

        }

        [HttpGet("Projects_TaskCount/{id}")]
        public async Task<ActionResult<Dictionary<ProjectDTO, int>>> GetProjectsTaskCountByUser(int id)
        {

            var dictionary = await _projectService.GetProjectTasksCountByUser(id);

            var json = JsonConvert.SerializeObject(dictionary, Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.None,
                TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple,
                ContractResolver = new DictionaryAsArrayResolver()
            });
            return Ok(json);

        }

        [HttpGet("DetailedInfo")]
        public async Task<ActionResult<IEnumerable<ProjectDetailedInfoDTO>>> GetProjectsInfo()
        {
            return Ok(await _projectService.GetProjectsInfo());
        }

    }
}