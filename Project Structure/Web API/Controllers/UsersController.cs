﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.Interfaces;
using Common.DTOs.User;
using Microsoft.AspNetCore.Mvc;

namespace Web_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<UserDTO>>> Get()
        {
            return Ok(await _userService.GetAllAsync());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDTO>> GetById(int id)
        {

            return Ok(await _userService.GetByIdAsync(id));

        }

        [HttpPost]
        public async Task<ActionResult<UserDTO>> Post([FromBody] UserCreateDTO userDTO)
        {

            var user = await _userService.CreateAsync(userDTO);
            return Created(new Uri($"api/Users/{user.Id}"), user);

        }

        [HttpPut]
        public async Task<ActionResult<UserDTO>> Put([FromBody] UserUpdateDTO userDTO)
        {

            return Ok(await _userService.UpdateAsync(userDTO));

        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteById(int id)
        {

            await _userService.DeleteByIdAsync(id);
            return NoContent();

        }

        [HttpGet("DetailedInfo/{id}")]
        public async Task<ActionResult<UserDetailedInfoDTO>> GetUserInfo(int id)
        {

            return Ok(await _userService.GetUserInfo(id));

        }
    }
}
