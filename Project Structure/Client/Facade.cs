﻿using Client.Interfaces;
using Client.Services;
using Common.DTOs.Project;
using Common.DTOs.Task;
using Common.DTOs.Team;
using Common.DTOs.User;
using Common.Enums;
using Common;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Collections.Generic;
using Nito.AsyncEx;

namespace Client
{
    public class Facade
    {
        private readonly IProjectService projectService;
        private readonly ITaskService taskService;
        private readonly IUserService userService;
        private readonly ITeamService teamService;
        private readonly AsyncLock asyncLock;
        private const int delay_ms = 1000;

        public Facade()
        {
            var httpclient = new HttpClient();
            projectService = new ProjectService(httpclient);
            taskService = new TaskService(httpclient);
            userService = new UserService(httpclient);
            teamService = new TeamService(httpclient);
            asyncLock = new AsyncLock();
        }

        public async Task PrintHeaderAsync()
        {
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Commands:");
                Console.WriteLine("1 - print tasks amount in specific user's projects");
                Console.WriteLine($"2 - print task list performed by user, where task name < {Constants.MaxNameLength} characters");
                Console.WriteLine($"3 - print task list finished in current({DateTime.Now.Year})");
                Console.WriteLine($"4 - print users older {Constants.MinAge} years, grouped by teams");
                Console.WriteLine("5 - print users sorted by first_name with tasks sorted by name length descending");
                Console.WriteLine("6 - print user's info");
                Console.WriteLine("7 - print projects info");
                Console.WriteLine("8 - create project");
                Console.WriteLine("9 - create user");
                Console.WriteLine("10 - create task");
                Console.WriteLine("11 - create team");
                Console.WriteLine("12 - update project");
                Console.WriteLine("13 - update user");
                Console.WriteLine("14 - update task");
                Console.WriteLine("15 - update team");
                Console.WriteLine("16 - delete project");
                Console.WriteLine("17 - delete user");
                Console.WriteLine("18 - delete task");
                Console.WriteLine("19 - delete team");
                Console.WriteLine($"20 - print tasks unfinished by user");
                Console.WriteLine($"21 - mark tasks as finished");
                Console.WriteLine("0 - exit");
            }
        }

        public async Task Start()
        {
            var queries = new List<Task>();
            //var isExit = false;

            var cycleTask = Task.Run(async () =>
            {
                await PrintHeaderAsync().ConfigureAwait(false);
                while (true)
                {
                    Console.WriteLine("Enter command:");
                    var commandString = Console.ReadLine();

                    try
                    {
                        int command = int.Parse(commandString);
                        Task query = null;

                        switch (command)
                        {
                            case 0:
                                return;
                            case 1:
                                query = PrintTaskCountInProjectsByUser();
                                break;
                            case 2:
                                query = PrintTasksPerformedByUser();
                                break;
                            case 3:
                                query = PrintFinishedTasksByUser();
                                break;
                            case 4:
                                query = PrintUsersGroupedByTeam();
                                break;
                            case 5:
                                query = PrintTasksGroupedByUser();
                                break;
                            case 6:
                                query = PrintUserInfo();
                                break;
                            case 7:
                                query = PrintProjectsInfo();
                                break;
                            case 8:
                                query = CreateProjectAsync();
                                break;
                            case 9:
                                query = CreateUserAsync();
                                break;
                            case 10:
                                query = CreateTaskAsync();
                                break;
                            case 11:
                                query = CreateTeamAsync();
                                break;
                            case 12:
                                query = UpdateProjectAsync();
                                break;
                            case 13:
                                query = UpdateUserAsync();
                                break;
                            case 14:
                                query = UpdateTaskAsync();
                                break;
                            case 15:
                                query = UpdateTeamAsync();
                                break;
                            case 16:
                                query = DeleteProjectAsync();
                                break;
                            case 17:
                                query = DeleteUserAsync();
                                break;
                            case 18:
                                query = DeleteTaskAsync();
                                break;
                            case 19:
                                query = DeleteTeamAsync();
                                break;
                            case 20:
                                query = PrintUnfinishedTasksByUser();
                                break;
                            case 21:
                                query = MarkTask();
                                break;
                            default:
                                throw new Exception("Unknown command!");
                        }
                        queries.Add(Task.Run(async () =>
                        {
                            try
                            {
                                await query;
                            }
                            catch (FormatException e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }
                            finally
                            {
                                await PrintHeaderAsync();
                            }
                        }));
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            });
            queries.Add(cycleTask);
            await Task.WhenAll(queries);
        }

        public static int ReadInt()
        {
            var idString = Console.ReadLine();
            return int.Parse(idString);
        }

        public static DateTime ReadDate()
        {
            var dateString = Console.ReadLine();
            return Convert.ToDateTime(dateString);
        }

        public static TaskState ReadState()
        {
            var stateString = Console.ReadLine();
            var state = int.Parse(stateString);

            if (state < (int)TaskState.Created || state > (int)TaskState.Canceled)
                throw new FormatException($"Unknown State \"{state}\"!");
            return (TaskState)state;
        }

        public async Task MarkTask()
        {
            var markedTaskId = await taskService.MarkRandomTaskWithDelay(delay_ms);
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Task with id = {markedTaskId} has been finished");
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task PrintTaskCountInProjectsByUser()
        {
            var userId = 0;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter user id:");
                userId = ReadInt();
            }
            var result = await projectService.GetProjects_TaskCountByUser(userId);
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Tasks amount in specific user({userId})'s projects:");
                foreach (var kvp in result)
                {
                    Console.WriteLine($"Project:\n{kvp.Key.ToString($"{"",-Constants.Indent}")}\n{"TaskCount:",-Constants.Indent}{kvp.Value}\n");
                }
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task PrintTasksPerformedByUser()
        {
            var userId = 0;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter user id:");
                userId = ReadInt();
            }

            var result = (await taskService.GetTasksPerformedByUser(userId)).ToList();
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Tasks performed by user({userId}), where task name < {Constants.MaxNameLength} characters:");
                foreach (var task in result)
                {
                    Console.WriteLine(task);
                }
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task PrintFinishedTasksByUser()
        {
            var userId = 0;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter user id:");
                userId = ReadInt();
            }

            var resut = (await taskService.GetFinishedTasksByUser(userId)).ToList();
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Tasks finished in current({DateTime.Now.Year}) by user({userId}):");
                foreach (var task in resut)
                {
                    Console.WriteLine(task);
                }
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task PrintUnfinishedTasksByUser()
        {
            var userId = 0;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter user id:");
                userId = ReadInt();
            }

            var result = (await taskService.GetUnfinishedTasksByUser(userId)).ToList();
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Tasks unfinished by user({userId}):");
                foreach (var task in result)
                {
                    Console.WriteLine(task);
                }
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task PrintUsersGroupedByTeam()
        {
            var result = (await teamService.GetTeamsWithUsers()).ToList();
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Users older {Constants.MinAge} years, grouped by teams:");
                foreach (var item in result)
                {
                    Console.WriteLine(item);
                }
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task PrintTasksGroupedByUser()
        {
            var result = (await taskService.GetTasksGroupedByUser()).ToList();
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine("Users sorted by first_name with tasks sorted by name length descending:");
                foreach (var item in result)
                {
                    Console.Write(item);
                }
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task PrintUserInfo()
        {
            var userId = 0;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter user id:");
                userId = ReadInt();
            }

            var userInfo = await userService.GetUserInfo(userId);

            Console.Write($"user({userId})'s info:\n{userInfo}");
        }

        public async Task PrintProjectsInfo()
        {
            var result = (await projectService.GetProjectsInfo()).ToList();
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine("Projects info:");
                foreach (var projectInfo in result)
                {
                    Console.WriteLine(projectInfo);
                }
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task DeleteProjectAsync()
        {
            int id;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter project id:");
                id = ReadInt();
            }

            await projectService.DeleteProjectByIdAsync(id);
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Project({id}) Deleted!");
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task DeleteUserAsync()
        {
            int id;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter user id:");
                id = ReadInt();
            }

            await userService.DeleteUserByIdAsync(id);
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"User({id}) Deleted!");
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task DeleteTeamAsync()
        {
            int id;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter team id:");
                id = ReadInt();
            }

            await teamService.DeleteTeamByIdAsync(id);
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Team({id}) Deleted!");
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task DeleteTaskAsync()
        {
            int id;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter task id:");
                id = ReadInt();
            }

            await taskService.DeleteTaskByIdAsync(id);
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Task({id}) Deleted!");
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task CreateProjectAsync()
        {
            int authorId, teamId;
            string name, description;
            DateTime deadline;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter AuthotId:");
                authorId = ReadInt();
                Console.WriteLine("Enter TeamId:");
                teamId = ReadInt();
                Console.WriteLine("Enter name:");
                name = Console.ReadLine();
                Console.WriteLine("Enter description:");
                description = Console.ReadLine();
                Console.WriteLine("Enter deadline in format dd/mm/yyyy:");
                deadline = ReadDate();
            }

            var result = await projectService.CreateProjectAsync(new ProjectCreateDTO
            {
                AuthorId = authorId,
                TeamId = teamId,
                Name = name,
                Description = description,
                Deadline = deadline
            });

            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Project created:\n{result}");
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task CreateUserAsync()
        {
            int? teamId;
            string fname, lname, email;
            DateTime birthday;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter TeamId (0 - skip team):");
                teamId = ReadInt();
                Console.WriteLine("Enter FirstName:");
                fname = Console.ReadLine();
                Console.WriteLine("Enter LastName:");
                lname = Console.ReadLine();
                Console.WriteLine("Enter Email:");
                email = Console.ReadLine();
                Console.WriteLine("Enter birthdate in format dd/mm/yyyy:");
                birthday = ReadDate();
            }

            var result = await userService.CreateUserAsync(new UserCreateDTO
            {
                TeamId = (teamId == 0) ? null : teamId,
                FirstName = fname,
                LastName = lname,
                Email = email,
                Birthday = birthday
            });
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"User created:\n{result}");
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task CreateTeamAsync()
        {
            string name;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter Name:");
                name = Console.ReadLine();
            }

            var result = await teamService.CreateTeamAsync(new TeamCreateDTO
            {
                Name = name
            });
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Team created:\n{result}");
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task CreateTaskAsync()
        {
            int projectId, performerId;
            string name, description;
            DateTime finished;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter ProjectId:");
                projectId = ReadInt();
                Console.WriteLine("Enter PerformerId:");
                performerId = ReadInt();
                Console.WriteLine("Enter name:");
                name = Console.ReadLine();
                Console.WriteLine("Enter description:");
                description = Console.ReadLine();
                Console.WriteLine("Enter FinishedAt in format dd/mm/yyyy:");
                finished = ReadDate();
            }

            var result = await taskService.CreateTaskAsync(new TaskCreateDTO
            {
                ProjectId = projectId,
                PerformerId = performerId,
                Name = name,
                Description = description,
                FinishedAt = finished
            });
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Task created:\n{result}");
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task UpdateProjectAsync()
        {
            int id, authorId, teamId;
            string name, description;
            DateTime deadline;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter Id:");
                id = ReadInt();
                Console.WriteLine("Enter AuthotId:");
                authorId = ReadInt();
                Console.WriteLine("Enter TeamId:");
                teamId = ReadInt();
                Console.WriteLine("Enter name:");
                name = Console.ReadLine();
                Console.WriteLine("Enter description:");
                description = Console.ReadLine();
                Console.WriteLine("Enter deadline in format dd/mm/yyyy:");
                deadline = ReadDate();
            }

            var result = await projectService.UpdateProjectAsync(new ProjectUpdateDTO
            {
                Id = id,
                AuthorId = authorId,
                TeamId = teamId,
                Name = name,
                Description = description,
                Deadline = deadline
            });
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Project updated:\n{result}");
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task UpdateUserAsync()
        {
            int id;
            int? teamId;
            string fname, lname, email;
            DateTime birthday;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter Id:");
                id = ReadInt();
                Console.WriteLine("Enter TeamId (0 - skip team):");
                teamId = ReadInt();
                Console.WriteLine("Enter FirstName:");
                fname = Console.ReadLine();
                Console.WriteLine("Enter LastName:");
                lname = Console.ReadLine();
                Console.WriteLine("Enter Email:");
                email = Console.ReadLine();
                Console.WriteLine("Enter birthdate in format dd/mm/yyyy:");
                birthday = ReadDate();
            }

            var result = await userService.UpdateUserAsync(new UserUpdateDTO
            {
                Id = id,
                TeamId = teamId == 0 ? null : teamId,
                FirstName = fname,
                LastName = lname,
                Email = email,
                Birthday = birthday
            });
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"User updated:\n{result}");
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task UpdateTeamAsync()
        {
            int id;
            string name;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter Id:");
                id = ReadInt();
                Console.WriteLine("Enter Name:");
                name = Console.ReadLine();
            }

            var result = await teamService.UpdateTeamAsync(new TeamUpdateDTO
            {
                Id = id,
                Name = name
            });
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Team updated:\n{result}");
                Console.WriteLine(new string('-', 140));
            }
        }

        public async Task UpdateTaskAsync()
        {
            int id, projectId, performerId;
            string name, description;
            DateTime finished;
            TaskState state;
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine("Enter Id:");
                id = ReadInt();
                Console.WriteLine("Enter ProjectId:");
                projectId = ReadInt();
                Console.WriteLine("Enter PerformerId:");
                performerId = ReadInt();
                Console.WriteLine("Enter name:");
                name = Console.ReadLine();
                Console.WriteLine("Enter description:");
                description = Console.ReadLine();
                Console.WriteLine("Enter FinishedAt in format dd/mm/yyyy:");
                finished = ReadDate();
                Console.WriteLine("Enter taskState 0 - Created 1 - Started 2 - Finished 3 - Canceled");
                state = ReadState();
            }

            var result = await taskService.UpdateTaskAsync(new TaskUpdateDTO
            {
                Id = id,
                ProjectId = projectId,
                PerformerId = performerId,
                Name = name,
                Description = description,
                FinishedAt = finished,
                State = state
            });
            using (await asyncLock.LockAsync())
            {
                Console.WriteLine(new string('-', 140));
                Console.WriteLine($"Task updated:\n{result}");
                Console.WriteLine(new string('-', 140));
            }
        }
    }
}