﻿using Common.DTOs.Task;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Client.Interfaces
{
    public interface ITaskService
    {
        Task<IEnumerable<TaskDTO>> GetTasksAsync();

        Task<TaskDTO> GetTaskByIdAsync(int id);

        Task<TaskDTO> CreateTaskAsync(TaskCreateDTO taskCreateModel);

        Task<TaskDTO> UpdateTaskAsync(TaskUpdateDTO taskUpdateModel);

        Task DeleteTaskByIdAsync(int id);

        Task<IEnumerable<TaskDTO>> GetTasksPerformedByUser(int id);

        Task<IEnumerable<TaskFinishedDTO>> GetFinishedTasksByUser(int id);

        Task<IEnumerable<UserWithTasksDTO>> GetTasksGroupedByUser();

        Task<IEnumerable<TaskDTO>> GetUnfinishedTasksByUser(int id);

        Task<int> MarkRandomTaskWithDelay(int delay);
    }
}
