﻿using Client.Interfaces;
using Client.Properties;
using Common.DTOs.Task;
using Common.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Timers;

namespace Client.Services
{
    public class TaskService : ITaskService
    {
        private readonly HttpClient _client;

        public TaskService(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<TaskDTO>> GetTasksAsync()
        {
            var tasks = await _client.GetStringAsync($"{Resources.BaseUrl}/api/Tasks");
            return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(tasks);
        }

        public async Task<TaskDTO> GetTaskByIdAsync(int id)
        {
            var response = await _client.GetAsync($"{Resources.BaseUrl}/api/Tasks/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<TaskDTO>();
        }

        public async Task<TaskDTO> CreateTaskAsync(TaskCreateDTO taskCreateModel)
        {
            var response = await _client.PostAsJsonAsync($"{Resources.BaseUrl}/api/Tasks", taskCreateModel);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<TaskDTO>();
        }

        public async Task<TaskDTO> UpdateTaskAsync(TaskUpdateDTO taskUpdateModel)
        {
            var response = await _client.PutAsJsonAsync($"{Resources.BaseUrl}/api/Tasks", taskUpdateModel);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<TaskDTO>();
        }

        public async Task DeleteTaskByIdAsync(int id)
        {
            var response = await _client.DeleteAsync($"{Resources.BaseUrl}/api/Tasks/{ id}");
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
        }

        public async Task<IEnumerable<TaskDTO>> GetTasksPerformedByUser(int id)
        {
            var response = await _client.GetAsync($"{Resources.BaseUrl}/api/Tasks/performedByUser/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<IEnumerable<TaskDTO>>();
        }

        public async Task<IEnumerable<TaskFinishedDTO>> GetFinishedTasksByUser(int id)
        {
            var response = await _client.GetAsync($"{Resources.BaseUrl}/api/Tasks/finishedByUser/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<IEnumerable<TaskFinishedDTO>>();
        }

        public async Task<IEnumerable<TaskDTO>> GetUnfinishedTasksByUser(int id)
        {
            var response = await _client.GetAsync($"{Resources.BaseUrl}/api/Tasks/unfinishedByUser/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<IEnumerable<TaskDTO>>();
        }

        public async Task<IEnumerable<TaskDTO>> GetUnfinishedTasks()
        {
            var tasks = await _client.GetStringAsync($"{Resources.BaseUrl}/api/Tasks/unfinished");
            return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(tasks);
        }

        public async Task<IEnumerable<UserWithTasksDTO>> GetTasksGroupedByUser()
        {
            var tasks = await _client.GetStringAsync($"{Resources.BaseUrl}/api/Tasks/groupedByUser");
            return JsonConvert.DeserializeObject<IEnumerable<UserWithTasksDTO>>(tasks);
        }

        public async Task<int> MarkRandomTaskWithDelay(int delay)
        {
            var tcs = new TaskCompletionSource<int>();

            var aTimer = new Timer { Interval = delay, AutoReset = false};
            aTimer.Elapsed += async (o, e) =>
            {
                try
                {
                    var tasks = await GetUnfinishedTasks();
  
                    if (!tasks.Any())
                    {
                        throw new Exception("All tasks are finisehd!");
                    }
                    var random = new Random();
                    var index = random.Next(tasks.Count());
                    var randomTask = tasks.ElementAtOrDefault(index);
                    var updatedtask = new TaskUpdateDTO()
                    {
                        Id = randomTask.Id,
                        Name = randomTask.Name,
                        Description = randomTask.Description,
                        FinishedAt = DateTime.Now,
                        State = TaskState.Finished,
                        PerformerId = randomTask.Performer.Id,
                        ProjectId = randomTask.ProjectId
                    };
                    await UpdateTaskAsync(updatedtask);

                    tcs.SetResult(updatedtask.Id);
                }
                catch (Exception ex)
                {
                    tcs.SetException(ex);
                }
            };
            aTimer.Start();
            return await tcs.Task;
        }
    }
}
