﻿using Client.Interfaces;
using Client.Properties;
using Common.DTOs.User;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client.Services
{
    public class UserService : IUserService
    {
        private HttpClient _client;

        public UserService(HttpClient client)
        {
            _client = client;
        }

        public async Task<IEnumerable<UserDTO>> GetUsersAsync()
        {
            var users = await _client.GetStringAsync($"{Resources.BaseUrl}/api/Users");
            return JsonConvert.DeserializeObject<IEnumerable<UserDTO>>(users);
        }

        public async Task<UserDTO> GetUserByIdAsync(int id)
        {
            var response = await _client.GetAsync($"{Resources.BaseUrl}/api/Users/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<UserDTO>();
        }

        public async Task<UserDTO> CreateUserAsync(UserCreateDTO userCreateModel)
        {
            var response = await _client.PostAsJsonAsync($"{Resources.BaseUrl}/api/Users", userCreateModel);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<UserDTO>();
        }

        public async Task<UserDTO> UpdateUserAsync(UserUpdateDTO userUpdateModel)
        {
            var response = await _client.PutAsJsonAsync($"{Resources.BaseUrl}/api/Users", userUpdateModel);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<UserDTO>();
        }

        public async Task DeleteUserByIdAsync(int id)
        {
            var response = await _client.DeleteAsync($"{Resources.BaseUrl}/api/Users/{ id}");
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
        }

        public async Task<UserDetailedInfoDTO> GetUserInfo(int id)
        {
            var response = await _client.GetAsync($"{Resources.BaseUrl}/api/Users/DetailedInfo/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode}: {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<UserDetailedInfoDTO>();
        }
    }
}
