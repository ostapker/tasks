﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Services.Abstract;
using DAL.Entities;
using DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DAL.UnitOfWork.Interfaces;
using Common.DTOs.Task;
using Common;
using BLL.Exceptions;

namespace BLL.Services
{
    public class TaskService : BaseService, ITaskService
    {
        private readonly IRepository<TaskEntity> _repository;
        private readonly IProjectService _projectService;
        private readonly IUserService _userService;

        public TaskService(IUnitOfWork unitOfWork, IMapper mapper, IProjectService projectService, IUserService userService) 
            : base(unitOfWork, mapper)
        {
            _repository = _unitOfWork.Set<TaskEntity>();
            _projectService = projectService;
            _userService = userService;
        }

        public async Task<TaskDTO> CreateAsync(TaskCreateDTO taskDTO)
        {
            var task = _mapper.Map<TaskEntity>(taskDTO);

            await _projectService.GetByIdAsync(taskDTO.ProjectId);
            await _userService.GetByIdAsync(taskDTO.PerformerId);

            task.CreatedAt = DateTime.Now;
            task.State = TaskState.Created;

            await _repository.CreateAsync(task);
            await _unitOfWork.SaveChangesAsync();

            return (await GetAllAsync()).FirstOrDefault(t => t.Id == task.Id);
        }

        public async Task DeleteByIdAsync(int id)
        {
            await GetByIdAsync(id);
            await _repository.DeleteByIdAsync(id);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IEnumerable<TaskDTO>> GetAllAsync()
        {
            var tasks = (await _repository.GetAllAsync())
                        .Include(t => t.Performer);
            return tasks.Select(task => _mapper.Map<TaskDTO>(task))
                        .AsEnumerable();
        }

        public async Task<TaskDTO> GetByIdAsync(int id)
        {
            var task = (await GetAllAsync())
                        .FirstOrDefault(t => t.Id == id);
            if (task == null)
                throw new NotFoundException("Task", id);
            return task;
        }

        public async Task<TaskDTO> UpdateAsync(TaskUpdateDTO taskDTO)
        {
            var task = await _repository.GetByIdAsync(taskDTO.Id);
            if (task == null)
                throw new NotFoundException("Task", taskDTO.Id);

            await  _projectService.GetByIdAsync(taskDTO.ProjectId);
            await _userService.GetByIdAsync(taskDTO.PerformerId);

            if (!Enum.IsDefined(typeof(TaskState), (TaskState)taskDTO.State))
                throw new UnknownTaskStateException();

            task.ProjectId = taskDTO.ProjectId;
            task.PerformerId = taskDTO.PerformerId;
            task.Name = taskDTO.Name;
            task.Description = taskDTO.Description;
            task.FinishedAt = taskDTO.FinishedAt;
            task.State = (TaskState)taskDTO.State;

            await _repository.UpdateAsync(task);
            await _unitOfWork.SaveChangesAsync();

            return (await GetAllAsync()).FirstOrDefault(t => t.Id == task.Id);
        }
        
        public async Task<IEnumerable<TaskDTO>> GetTasksPerformedByUser(int userId)
        {
            await _userService.GetByIdAsync(userId);

            var tasks = await GetAllAsync();
            return tasks.Where(task => task.PerformerId == userId && task.Name.Length < Constants.MaxNameLength);
        }

        public async Task<IEnumerable<TaskFinishedDTO>> GetFinishedTasksByUser(int userId)
        {
            await _userService.GetByIdAsync(userId);

            var tasks = await GetAllAsync();
            return tasks.Where(task => task.PerformerId == userId && (int)task.State == (int)TaskState.Finished && task.FinishedAt.Year == DateTime.Now.Year)
                                .Select(task => _mapper.Map<TaskFinishedDTO>(task));

        }
        
        public async Task<IEnumerable<UserWithTasksDTO>> GetTasksGroupedByUser()
        {
            var tasks = await GetAllAsync();
            var users = await _userService.GetAllAsync();

            return users.OrderBy(u => u.FirstName)
                .GroupJoin(tasks.OrderByDescending(t => t.Name.Length),
                    user => user.Id,
                    task => task.PerformerId,
                    (user, tasks) => new UserWithTasksDTO
                    { 
                        User = user,
                        Tasks = tasks
                    });
        }

        public async Task<IEnumerable<TaskDTO>> GetUnfinishedTasksByUser(int userId)
        {
            await _userService.GetByIdAsync(userId);
            var tasks = await GetAllAsync();
            return tasks.Where(t => t.PerformerId == userId && (int)t.State != (int)TaskState.Finished);
        }

        public async Task<IEnumerable<TaskDTO>> GetUnfinishedTasks()
        {
            var tasks = await GetAllAsync();
            return tasks.Where(t =>(int)t.State != (int)TaskState.Finished);
        }

        public void Dispose()
        {
            _userService.Dispose();
            _projectService.Dispose();
            _unitOfWork.Dispose();
        }
    }
}
